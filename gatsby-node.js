const axios = require('axios').default
const parse = require('csv-parse/lib/sync')
const path = require('path')

exports.onCreateNode = async ({ node, actions }) => {
  const { createNodeField } = actions

  if (node.internal.type === `Airtable` && node.table === `Tests` && node.data.Results) {
    await axios.get(node.data.Results[0].url)
      .then(res => res.data)
      .then(parse)
      .then(data => {
          // figure out the columns
          const escSignal = data[0].findIndex(col => col === 'ESC signal (µs)')
          const thrust = data[0].findIndex(col => col === 'Thrust (gf)')
          const current = data[0].findIndex(col => col === 'Current (A)')
          const voltage = data[0].findIndex(col => col === 'Voltage (V)')
          // const eRPM = data[0].findIndex(col => col === 'Motor Electrical Speed (RPM)')
          // const oRPM = data[0].findIndex(col => col === 'Motor Optical Speed (RPM)')

          // [escSignal[], thrust[], current[], voltage[]]
          // calculate Electrical Power (W) and Overall Efficiency (gf/W) in the frontend
          const results = [[], [], [], []]

          data.forEach((row, index) => {
            // skip headers and zero throttle
            if (!(index === 0 || row[escSignal] === `1000`)) {
              results[0].push(Number(parseFloat(row[escSignal]).toFixed(2)))
              results[1].push(Number(parseFloat(row[thrust]).toFixed(2)))
              results[2].push(Number(parseFloat(row[current]).toFixed(2)))
              results[3].push(Number(parseFloat(row[voltage]).toFixed(2)))
              // results[4].push(parseInt(row[eRPM]))
              // results[5].push(parseInt(row[oRPM]))
            }
          })

          createNodeField({
            node,
            name: `results`,
            value: results,
          })
      })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const result = await graphql(`
    query {
      allContentfulArticle {
        nodes {
          slug
        }
      }
    }
  `)

  result.data.allContentfulArticle.nodes.forEach(({ slug }) => {
    createPage({
      path: `articles/${slug}`,
      component: path.resolve('./src/templates/article.js'),
      context: {
        slug,
      },
    })
  })
}