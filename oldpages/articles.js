import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

export default ({ data }) => {
  return (
    <Layout wrapperClass="Articles">
      <SEO title="Articles" />
      <section className="section">
        <div className="container">
          <h1 className="title">Articles</h1>
          <ul>
            {data.allContentfulArticle.nodes.map(({ title, slug }) => (
              <li key={slug}>
                <a href={'/articles/' + slug}>{title}</a>
              </li>
            ))}
          </ul>
        </div>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query {
    allContentfulArticle(filter: {isVisible: {eq: true}}) {
      nodes {
        title
        slug
      }
    }
  }
`