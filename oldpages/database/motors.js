import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"

export default () => {
  return (
    <Layout wrapperClass="Motors">
      <SEO title="Motors" />
      <section className="section">
        <div className="container">
          <h1 className="title">Motors</h1>
          <p className="subtitle">Sorted by stator and KV</p>

          <iframe
            title="Motors"
            className="airtable-embed"
            src="https://airtable.com/embed/shrV8R6RThJURXane?backgroundColor=teal&viewControls=on"
            frameborder="0"
            onmousewheel=""></iframe>
        </div>
      </section>
    </Layout>
  )
}