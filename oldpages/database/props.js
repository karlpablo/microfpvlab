import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"

export default () => {
  return (
    <Layout wrapperClass="Props">
      <SEO title="Props" />
      <section className="section">
        <div className="container">
          <h1 className="title">Props</h1>
          <p className="subtitle">Sorted by diameter and pitch</p>

          <iframe
            title="Props"
            className="airtable-embed"
            src="https://airtable.com/embed/shrljfOUC95Sv0tZh?backgroundColor=teal&viewControls=on"
            frameborder="0"
            onmousewheel=""></iframe>
        </div>
      </section>
    </Layout>
  )
}