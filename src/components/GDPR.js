import React, { useState } from "react"

export default () => {
  const [accepted, setAccepted] = useState(false)

  return (
    <div className={ 'GDPR modal' + (!accepted ? ' is-active' : '')}>
      <div className="modal-background" role="none"></div>
      <div className="modal-content">
        <h1 className="title has-text-centered">This website uses cookies</h1>
        <p>We use cookies to personalise content and ads, to provide social media features and to analyse our traffic. We also share information about your use of our site with our social media, advertising and analytics partners who may combine it with other information that you’ve provided to them or that they’ve collected from your use of their services.</p>
        <button onClick={() => setAccepted(true)}>I accept</button>
      </div>
    </div>
  )
}