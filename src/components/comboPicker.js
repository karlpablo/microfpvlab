import React, { useState } from "react"

export default ({ allCombos, slots, isVisible, hide, selectCombo }) => {
  // Re-shape the allCombos so we can use them for the dropdowns
  // Create an array of all UNIQUE and AVAILABLE batteries from the combos
  let batteries = []

  allCombos.forEach(combo => {
    let batteryIndex = batteries.findIndex(battery => battery.Name === combo.battery.Name)

    // If battery does not exist, create it and keep track of the new index
    if (batteryIndex === -1) {
      batteryIndex = batteries.push({
        motors: [],
        ...combo.battery,
      }) - 1
    }

    const thisBattery = batteries[batteryIndex]

    // Now, we create 2nd level of grouping (motors)
    let motorIndex = thisBattery.motors.findIndex(motor => motor.Name === combo.motor.Name)

    if (motorIndex === -1) {
      motorIndex = thisBattery.motors.push({
        combos: [],
        ...combo.motor,
      }) - 1
    }

    const thisMotor = thisBattery.motors[motorIndex]

    // Now, we add the actual combo to this battery's motors' combos[]
    thisMotor.combos.push(combo)
  })

  // sort by battery Name
  batteries = batteries.sort((a, b) => a.Name < b.Name ? -1 : 1)

  // sort motors and props, too
  batteries.forEach(battery => {
    battery.motors = battery.motors.sort((a, b) => a.Name.toLowerCase() < b.Name.toLowerCase() ? -1 : 1)

    battery.motors.forEach(motor => {
      motor.combos = motor.combos.sort((a, b) => a.prop.Name.toLowerCase() < b.prop.Name.toLowerCase() ? -1 : 1)
    })
  })

  // create state for knowing expanded status of each battery and motors in each battery, rather than making the whole object a state
  const [batteryState, setBatteryState] = useState(batteries.map(battery => ({
    isExpanded: false,
    motors: battery.motors.map(motor => ({ isExpanded: false }))
  })))

  const toggleBatteryState = (batteryIndex) => {
    batteryState[batteryIndex].isExpanded = !batteryState[batteryIndex].isExpanded
    setBatteryState([...batteryState])
  }

  const toggleMotorState = (motorIndex, batteryIndex) => {
    batteryState[batteryIndex].motors[motorIndex].isExpanded = !batteryState[batteryIndex].motors[motorIndex].isExpanded
    setBatteryState([...batteryState]) 
  }

  return (
    <div className={ 'comboPicker modal' + (isVisible ? ' is-active' : '')}>
      <div className="modal-background" onClick={hide} role="none"></div>
      <div className="modal-content">
        <button className="modal-close is-large" aria-label="close" onClick={hide}></button>

        <h1 className="title has-text-centered">Select a combo</h1>

        { batteries.map((battery, batteryIndex) => (
            <div className="batteryGroup" key={battery.Name}>
              <button className="batteryGroup__title" onClick={() => toggleBatteryState(batteryIndex)}>
                {battery.Name} ({battery.Voltage}V)
              </button>
          
              <div className={ 'batteryGroup__contents' + (batteryState[batteryIndex].isExpanded ? ' -isExpanded' : '')}>
                { battery.motors.map((motor, motorIndex) => (
                    <div className="motorGroup" key={motorIndex}>
                      <button className="motorGroup__title" onClick={() => toggleMotorState(motorIndex, batteryIndex)}>
                        {motor.Name}
                      </button>
                      <table className={ 'motorGroup__table table is-fullwidth is-striped is-hoverable' + (batteryState[batteryIndex].motors[motorIndex].isExpanded ? ' -isExpanded' : '')}>
                        <tbody>
                          { motor.combos.map((combo, comboIndex) => (
                              <tr key={comboIndex}>
                                <td>
                                  {combo.prop.Name}
                                </td>
                                <td>
                                  { combo.notes || '-' }
                                </td>
                                <td>
                                  { slots.find(slot => slot.combo && slot.combo.id === combo.id) ? (
                                      <span>Selected</span>
                                    ) : (
                                      <button onClick={() => selectCombo(combo)}>Select</button>
                                    )
                                  }
                                </td>
                              </tr>
                            ))
                          }
                        </tbody>
                      </table>
                    </div>
                  ))
                }
              </div>
            </div>
          ))
        }
      </div>
    </div>
  )
}