import React from "react"

export default ({ slots, select, clear }) => {
  return (
    <div className="comboSlots">
      { slots.map((slot, index) => (
        <div
          className="combo"
          key={index}
          style={{ backgroundColor: slot.backgroundColor }}
          onClick={() => clear(index)}
        >
          { slot.combo === null ? (
            <button className="button is-white is-rounded" onClick={() => select(index)}>
              <span>Select...</span>
            </button>
          ) : (
            <>
              <table
                className="combo__table table is-narrow is-bordered is-fullwidth"
                style={{ color: slot.textColor, borderColor: slot.textColor }}>
                <thead>
                  <tr>
                    <th colSpan="6">
                      <h2 className="title is-6"
                         style={{ color: slot.textColor }}>
                        {slot.combo.motor.Name} + {slot.combo.prop.Name} @ {slot.combo.battery.Name} ({slot.combo.battery.Voltage}V)
                      </h2>
                    </th>
                  </tr>
                  <tr className="statHeaders">
                    <th>ESC Signal (µs)</th>
                    <th>Thrust (gf)</th>
                    <th>Current (A)</th>
                    <th>Voltage (V)</th>
                    <th>Power (W)</th>
                    <th>Efficiency (gf/W)</th>
                  </tr>
                </thead>
                <tbody>
                  { slot.combo.results[0].map((escSignal, index) => {
                      const thrust = parseFloat(slot.combo.results[1][index]).toFixed(1)
                      const current = parseFloat(slot.combo.results[2][index]).toFixed(1)
                      const voltage = parseFloat(slot.combo.results[3][index]).toFixed(1)

                      return (
                        <tr key={index}>
                          <td>{escSignal}</td>
                          <td>{thrust}</td>
                          <td>{current}</td>
                          <td>{voltage}</td>
                          <td>{parseFloat(current * voltage).toFixed(1)}</td>
                          <td>{parseFloat(thrust / (current * voltage)).toFixed(1)}</td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>

              { slot.combo.notes && slot.combo.notes.length > 0 ? (
                  <blockquote className="combo__notes" style={{ color: slot.textColor }}>
                    Notes: {slot.combo.notes}
                  </blockquote>
                ) : null
              }
            </>
          )}
        </div>
      ))}
    </div>
  )
}