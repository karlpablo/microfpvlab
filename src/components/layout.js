import React, { useState } from "react"
import { Link, useStaticQuery, graphql } from "gatsby"

const Layout = ({ children, wrapperClass }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  const [isOpen, setIsOpen] = useState(false)

  return (
    <>
      <header className="header">
        <nav className="navbar is-black" role="navigation" aria-label="main navigation">
          <div className="container">
            <div className="navbar-brand">
              <Link className="navbar-item" to="/" >
                <h1 className="title is-5 has-text-white">{data.site.siteMetadata.title}</h1>
              </Link>

              <button
                className={'navbar-burger burger' + (isOpen ? ' is-active' : '')}
                aria-label="menu"
                aria-expanded="false"
                onClick={() => setIsOpen(!isOpen)}>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
              </button>
            </div>

            <div className={'navbar-menu' + (isOpen ? ' is-active' : '')}>
              <div className="navbar-start"></div>

              <div className="navbar-end">
                <a className="navbar-item" href="https://www.facebook.com/groups/microfpvlab/" target="_blank" rel="noreferrer">
                  Join our Facebook group!
                </a>
              </div>
            </div>
          </div>
        </nav>
      </header>
      <main className={wrapperClass}>{children}</main>
      <footer className="footer">
        <div className="container has-text-right">
          <p>© {new Date().getFullYear()} - Built by <a href="https://karlpablo.com" target="_blank" rel="noreferrer">Karl Pablo</a> using ReactJS, Gatsby and Bulma.</p>
          <p>Deployed and hosted on Netlify.</p>
        </div>
      </footer>
    </>
  )
}

export default Layout
