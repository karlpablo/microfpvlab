import React from "react"
import { Bar } from "react-chartjs-2"

export default ({ chartData }) => {
  const chartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    title: {
      display: false,
      position: 'bottom',
      text: [
        'Lines indicate thrust values in grams, following scale on the left side.',
        'Bars indicate current draw in amperes, following scale on the right side.',
        'Bottom scale indicates throttle points. Some combos do not have complete points.'
      ]
    },
    tooltips: {
      // enabled: false,
      callbacks: {
        title: (tooltipItem, data) => {
          return `At ${tooltipItem[0].label} throttle:`
        },
        // label: (tooltipItem, data) => {
        //   console.log(tooltipItem, data)
        //   return [
        //     ` Thrust: ${data.datasets[0].data[tooltipItem.index]} g/f`,
        //     ` Current: ${data.datasets[1].data[tooltipItem.index]} A`
        //   ]
        // }
      }
    },
    elements: {
      line: {
        fill: false,
        tension: 0.4,
        borderWidth: 1
      },
      rectangle: {
        borderWidth: 0,
      },
      point: {
        radius: 3,
        hoverRadius: 3,
        pointStyle: 'circle'
      }
    },
    scales: {
      xAxes: [
        {
          id: 'escSignal-axis',
          type: 'linear',
          ticks: {
            stepSize: 100,
            min: 1000,
            max: 2000,
          },
          display: true,
          position: 'bottom',
          gridLines: {
            display: true
          },
          scaleLabel: {
            display: true,
            labelString: `ESC Signal (µs)`,
            fontFamily: `Patua One`,
            fontColor: `#000000`,
            fontSize: 16
          },
        },
      ],
      yAxes: [
        {
          id: 'thrust-axis',
          type: 'linear',
          display: true,
          position: 'left',
          gridLines: {
            display: true,
            // color: 'rgba(0, 0, 0, 1)'
          },
          labels: {
            show: false
          },
          scaleLabel: {
            display: true,
            labelString: 'Solid lines: Thrust (gf)',
            fontFamily: `Patua One`,
            fontColor: `#000000`,
            fontSize: 16
          },
        },
        {
          id: 'current-axis',
          type: 'linear',
          display: true,
          position: 'right',
          gridLines: {
            display: false,
            // color: 'rgba(0, 0, 0, 0.25)'
          },
          labels: {
            show: true
          },
          scaleLabel: {
            display: true,
            labelString: 'Dashed lines: Current (A)',
            fontFamily: `Patua One`,
            fontColor: `#000000`,
            fontSize: 16
          },
        }
      ]
    }
  }

  const chartPlugins = [{
    afterDraw: (chartInstance, easing) => {
      const ctx = chartInstance.chart.ctx
      ctx.font = '16px Patua One'
      ctx.fillStyle = 'rgba(0, 0, 0, 0.1)'
      ctx.fillText('MicroFPVLab.com', 80, 40) // todo: use graphql to pull this
    }
  }]

  return (
    <div className="mixedChart">
      <Bar
        data={chartData}
        options={chartOptions}
        plugins={chartPlugins}
      />
    </div>
  )
}