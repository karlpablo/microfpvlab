import React from "react"

export default () => (
  <div className="thrustTestInfo columns is-multiline">
    <div className="column">
      <h2 className="title is-5">Stand Setup</h2>
      <ul>
        <li><strong>Thrust Stand:</strong> RCBenchmark 1520, with custom CF motor mounting arms</li>
        <li><strong>Load cell:</strong> Rated for up to 2-kilogram</li>
        <li><strong>ESC:</strong>
          <ul>
            <li>1-2S: HAKRC 10Ax4, BLHELI_S 16.7</li>
            <li>2-6S: Spedix GS35 35A, BLHELI_32</li>
          </ul>
        </li>
        <li><strong>Power Supply:</strong> TEKPOWER TP3030E (0-30V, 0-30A)</li>
        <li><strong>Thermometer:</strong> Proster Digital, thermocouple directly touching motor windings</li>
        <li><strong>Wires:</strong>
          <ul>
            <li>Thrust stand to ESC: 18AWG XT30</li>
            <li>ESC to WhitenoiseFPV racewire: 22AWG</li>
            <li>Racewire to motors: stock motor wires, soldered</li>
          </ul>
        </li>
        <li><strong>Prop Direction:</strong> Pusher type (forward/away) to prevent airflow impedance of thrust hardware</li>
        <li><strong>Cooling:</strong> None during, only in between tests. After several trials, it was found that any sort of airflow behind the thrust stand will significantly interfere with the thrust readings and will actually result in lower and highly inconsistent values.</li>
      </ul>
    </div>
    <div className="column">
      <h2 className="title is-5">Test Script</h2>
      <ol>
        <li>Tare load cell to 0.</li>
        <li>Initialize ESC. Set throttle to 0%.</li>
        <li>Ramp motors up by 10%.</li>
        <li>Let stabilize for 2 seconds at current throttle.</li>
        <li>Take 100x readings (at 40Hz sampling rate) and collect averaged results.</li>
        <li>If throttle is at 60%<sup>*</sup> or higher:
          <ol>
            <li>Ramp down to 15%<sup>*</sup> throttle.</li>
            <li>Cooldown for 20<sup>*</sup> seconds.</li>
          </ol>
        </li>
        <li>Repeat Step 3, increasing throttle up to 100%.</li>
      </ol>

      <p>Raw script can be found <a href="https://bitbucket.org/karlpablo/workspace/snippets/qnMGoR/thrust-stand-test-script" target="_blank" rel="noreferrer">here</a>.</p>

      <p><sup>*</sup> Depending on motor core temperature, these variables are adjusted and the test is repeated until it can hit 100% throttle without overheating, if possible. Otherwise, the test will be ended prematurely.</p>
    </div>
    <div className="column">
      <h2 className="title is-5">Notes</h2>
      <p><strong>Faulty Motors:</strong> Unless noted otherwise and due to time constraints, each test is performed on one new or lightly used motor per combo. If there are huge discrepancies against similarly-spec'd combos, I will repeat the test using another motor from the same set.</p>
      <p><strong>Incomplete Throttle Points:</strong> The motor temperature is constantly monitored during testing using a thermocouple probe directly touching the windings. In some cases where the prop is "too much" for a motor resulting in overheating (even with cooling in-between throttle points), the test is stopped and the results from the previous throttle points are still collected. This does NOT mean the prop cannot be used for that motor! Static environments are naturally prone to heat up more because of lack of real airflow.</p>
      <p><strong>Prop Screws:</strong> All combos will be tested with prop screws on, unless the motor or prop do not have mounting holes. This is due to some props slipping off on high throttle points, even when fitted with dental floss.</p>
      <p><strong>Motor Weight:</strong> Values are just are rounded up and are just estimates. Some manufacturers include the wires with the total weight, some do not. Please check with them for more accuracy.</p>
    </div>
  </div>
)