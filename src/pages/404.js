import React from "react"
import SEO from "../components/seo"
import { Link } from "gatsby"

const NotFoundPage = () => (
  <>
    <SEO title="404: Not found" />
    <section className="hero is-warning is-fullheight">
      <div className="hero-body">
        <div className="container">
          <h1 className="title">Oops, that page does not exist.</h1>
          <p  className="subtitle">
            Try starting over from <Link to={'/'} className="has-text-link">the home page</Link>.
          </p>
        </div>
      </div>
    </section>
  </>
)

export default NotFoundPage
