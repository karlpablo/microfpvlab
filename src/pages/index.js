import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const CATEGORIES = [
  'Tests',
  'Comparisons',
  // 'Reviews',
  'R&Ds',
  'Builds',
  'Guides',
]

const FLAGS = {
  'New': {
    tagClass: 'is-primary',
    text: 'New!',
  },
  'Updated': {
    tagClass: 'is-info',
    text: 'Updated!',
  },
  'In Progress': {
    tagClass: 'is-warning',
    text: 'In Progress',
  },
  'Upcoming': {
    tagClass: 'is-warning is-light',
    text: 'Coming soon..',
  },
  'Needs Funding': {
    tagClass: 'is-danger',
    text: 'Needs Funding!',
  },
  'Funded': {
    tagClass: 'is-success',
    text: 'Funded!',
  },
}

export default ({ data }) => {
  const articles = data.allContentfulArticle.nodes

  const renderArticles = (thisCategory) => {
    return articles
      .filter(({ category }) => category === thisCategory)
      .map(({ title, slug, excerpt, flag, featuredImage }) => (
        <div
          className="column is-half-tablet
            is-one-third-desktop is-one-quarter-widescreen"
          key={slug}
        >
          <div className="card">
            <div className="card-image">
              <figure
                className="image is-3by2"
                style={{ backgroundImage:
                  `url(${featuredImage ?
                    featuredImage.fixed.src :
                    'https://placekitten.com/600/400'})` }}
              />
              {flag ? 
                <span className={'tag '+FLAGS[flag].tagClass}>
                  {FLAGS[flag].text}
                </span>
              : null}
            </div>
            <div className="card-content">
              <div className="content">
                <h3>
                  {flag === 'Upcoming' ? title :
                    <Link to={'/articles/' + slug}>
                      {title}
                    </Link>
                  }
                </h3>
                <p>{excerpt}</p>
              </div>
            </div>
          </div>
        </div>
      ))
  }

  return (
    <Layout wrapperClass="Home">
      <SEO title="Home" />
      <section className="hero is-primary is-bold">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              MicroFPVLab
            </h1>
            <h2 className="subtitle">
              By <a href="https://karlpablo.com" target="_blank" rel="noreferrer">Karl Pablo</a>
            </h2>
          </div>
        </div>
      </section>
      {CATEGORIES.map((category, index) => (
        <section
          key={index}
          className={`section ${index % 2 === 0 ? '' : 'has-background-light'}`}
        >
          <div className="container">
            <h2 className="title is-2">{category}</h2>
            <div className="columns is-multiline">
              {renderArticles(category)}
            </div>
          </div>
        </section>
      ))}
    </Layout>
  )
}

export const query = graphql`
  query {
    allContentfulArticle(sort: {fields: updatedAt, order:DESC}) {
      nodes {
        title
        slug
        category
        excerpt
        updatedAt(fromNow: true)
        flag
        featuredImage {
          fixed(width: 640, quality: 90) {
            src
          }
        }
      }
    }
  }
`