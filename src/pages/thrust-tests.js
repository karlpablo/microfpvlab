import React, { useState, useEffect, useCallback } from "react"
import { graphql } from "gatsby"
import MixedChart from "../components/mixedChart"
import ComboSlots from "../components/comboSlots"
import ComboPicker from "../components/comboPicker"

export default ({ data }) => {
  const allCombos = data.allAirtable.nodes.map(item => ({
    id: item.id,
    testID: item.data.ID,
    motor: item.data.Motor[0].data,
    prop: item.data.Prop[0].data,
    battery: item.data.Battery[0].data,
    notes: item.data.Notes,
    results: item.fields ? item.fields.results : [[], []],
  }))

  const [chartData, setChartData] = useState({
    datasets: []
  })

  // pass '#FFFFFF' string
  const textColorToUse = (backgroundColor) => {
    // read as base16
    let r = parseInt(backgroundColor.substring(1, 3), 16)
    let g = parseInt(backgroundColor.substring(3, 5), 16)
    let b = parseInt(backgroundColor.substring(5, 7), 16)
    return ((r * 0.299) + (g * 0.587) + (b * 0.114) > 146) ?
      '#000000' : '#FFFFFF'
  }

  // x4 colorblind-safe
  const backgroundColors = [
    '#D55E00',
    '#F0E442',
    '#009E73',
    '#56B4E9',
  ]

  const [slots, setSlots] = useState(
    backgroundColors.map(color => ({
      combo: null,
      textColor: textColorToUse(color),
      backgroundColor: color
    }))
  )
  
  // clicking on a Slot button will save slot index here for the Combo Picker modal to populate
  const [activeSlot, setActiveSlot] = useState(null)

  const [hasPreloaded, setHasPreloaded] = useState(false)

  const clearSlot = (slotIndex) => {
    slots[slotIndex].combo = null
    redrawChart()
  }

  const selectCombo = (combo) => {
    console.log(combo)

    slots[activeSlot].combo = combo
    setActiveSlot(null) // hide the modal
    redrawChart()
  }

  const redrawChart = useCallback(() => {
    let newDatasets = []

    /*
    const generateData = (escSignalsTested, dataGathered) => {
      return escSignals.map(signal => {
        const i = escSignalsTested.findIndex(s => s === parseInt(signal))
        return i >= 0 ? { x: signal, y: dataGathered[i] } : null
        // return i >= 0 ? parseFloat(dataGathered[i]).toFixed(1) : null
      })
    }
    */

    const generateDataset = (index, combo, color) => {
      return ({
        thrust: {
          type:'line',
          label: `Combo ${index+1} (Thrust)`,
          data: combo.results[0].map((escSignal, index) => ({ x: escSignal, y: combo.results[1][index]})),
          borderColor: color,
          borderWidth: 2,
          pointRadius: 2,
          backgroundColor: color,
          pointBorderColor: color,
          pointBackgroundColor: color,
          yAxisID: 'thrust-axis'
        },
        current: {
          type: 'line',
          label: `Combo ${index+1} (Current)`,
          data: combo.results[0].map((escSignal, index) => ({ x: escSignal, y: combo.results[2][index]})),
          borderColor: color,
          borderWidth: 2,
          pointRadius: 2,
          backgroundColor: color,
          pointBorderColor: color,
          pointBackgroundColor: color,
          borderDash: [3],
          yAxisID: 'current-axis'
        }
      })
    }

    slots.forEach((slot, index) => {
      if (slot.combo !== null) {
        let {thrust, current} = generateDataset(index, slot.combo, slot.backgroundColor)
        newDatasets.push(thrust)
        newDatasets.push(current)
      }
    })

    // replace with new data
    setSlots([...slots])
    setChartData({ datasets: newDatasets })

    // scroll up
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
  }, [slots])

  useEffect(() => {
    if (hasPreloaded) {
      // update the URL
      const url = `${window.location.protocol}//${window.location.host}${window.location.pathname}`;
      const params = [];

      slots.forEach(slot => {
        if (slot.combo !== null) {
          params.push(slot.combo.testID.toString())
        }
      })

      if (params.length > 0) {
          window.history.replaceState(null, null, `${url}?combos=${params.join(':')}`);    
      } else {
          window.history.replaceState(null, null, url); 
      }
    } else {
      if (window.location.search.length > 0) {
        const url = new URL(window.location.href).searchParams;
        let combos = url.get('combos') ? url.get('combos').split(':') : []

        if (combos.length > 0) {
          combos = [...new Set(combos)] // make unique
            .filter(combo => combo.length > 0)
            .slice(0, slots.length) // get only first 4
            .map(combo => parseInt(combo)) // make integers

          combos.forEach((id, index) => {
            const thisCombo = allCombos.find(existingCombo => existingCombo.testID === id)

            if (thisCombo !== null) {
              slots[index].combo = thisCombo
            }
          })

          redrawChart()
        }
      }

      setHasPreloaded(true)
    }
  }, [hasPreloaded, allCombos, slots, redrawChart])

  return (
    <div className="ThrustTests">
      <section className="section">
        <div className="container">
          <MixedChart
            chartData={chartData}
          />

          <ComboSlots
            slots={slots}
            select={(index) => setActiveSlot(index)}
            clear={clearSlot}
          />

          <ComboPicker
            allCombos={allCombos}
            slots={slots} // to check if a combo was already selected
            isVisible={activeSlot !== null}
            hide={() => setActiveSlot(null)}
            selectCombo={selectCombo}
          />
        </div>
      </section>
    </div>
  )
}

export const query = graphql`
  query {
    allAirtable(filter: {table: {eq: "Tests"}, data: {Visible: {eq: true}}}) {
      nodes {
        id
        data {
          Motor {
            data {
              Name
              Weight
              Photo {
                url
              }
            }
          }
          Prop {
            data {
              Name
              Weight
              Photo {
                url
              }
            }
          }
          Battery {
            data {
              Name
              Voltage
            }
          }
          Notes
          ID
        }
        fields {
          results
        }
      }
    }
  }
`