import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

export default ({ data }) => {
  const article = data.contentfulArticle

  return (
    <Layout wrapperClass="Article">
      <SEO title={article.title} />
      <section className="hero">
        <div className="hero-body" style={{ backgroundImage: `url(${article.featuredImage.fixed.src})` }}>
          <div className="container is-readable-width">
            <h1 className="title has-text-white">
              {article.title}
            </h1>
            <h2 className="subtitle has-text-white">
              {article.excerpt}
            </h2>
          </div>
        </div>
      </section>
      <section className="section">
        <div className="container is-readable-width">
          {article.body ? 
            <div className="content" dangerouslySetInnerHTML={{ __html: article.body.childMarkdownRemark.html }} />
          : null}
          <hr />
          <p className="has-text-centered">
            <Link to="/">&larr; More articles</Link>
          </p>
        </div>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    contentfulArticle(slug: {eq: $slug}) {
      title
      excerpt
      body {
        childMarkdownRemark {
          html
        }
      }
      featuredImage {
        fixed(width: 1344, quality: 90) {
          src
        }
      }
    }
  }
`